document.addEventListener('DOMContentLoaded', function() {
  var fayeClient = new Faye.Client('http://192.168.20.93:3000/faye');

  document.getElementById('btnGo').addEventListener('click', function () {
    fayeClient.publish('/arduino/motor/set', {
        left: {
          speed: parseInt(document.getElementById('inputLeftMotorPower').value),
          direction: parseInt(document.getElementById('inputLeftMotorDirection').value),
          time: parseInt(document.getElementById('inputLeftMotorTime').value)},
        right: {
          speed: parseInt(document.getElementById('inputRightMotorPower').value),
          direction: parseInt(document.getElementById('inputRightMotorDirection').value),
          time: parseInt(document.getElementById('inputRightMotorTime').value)}});
  });
  document.getElementById('btnCalibrate').addEventListener('click', function () {
    fayeClient.publish('/arduino/imu/calibrate', {});
  });
  document.getElementById('btnZeroOdometry').addEventListener('click', function () {
    fayeClient.publish('/arduino/odometry/zero', {});
  });
  fayeClient.subscribe('/arduino/sonar', function (msg) {
    if (msg.sensor === 2) {
      document.getElementById('sonar1Text').innerText = msg.distance / 10;
      document.getElementById('sonar1').value = msg.distance / 10;
    } else if (msg.sensor === 3) {
      document.getElementById('sonar2Text').innerText = msg.distance / 10;
      document.getElementById('sonar2').value = msg.distance / 10;
    }
  });
  fayeClient.subscribe('/arduino/accuVoltage', function (msg) {
    document.getElementById('accuVoltage').innerText = msg.value + " mV";
  });

  setInterval(function () {
    fayeClient.publish('/opencv/distanceimage/get', {});
  //}, 3000);
  //setInterval(function () {
    document.getElementById('imgDistance').src = 'distanceSensor.png?time=' + (new Date()).getTime();
    document.getElementById('imgWebcam').src = 'webcamImage.png?time=' + (new Date()).getTime();
  }, 1000);
});
