// taken from https://stackoverflow.com/questions/10970958/get-a-color-component-from-an-rgb-string-in-javascript
var RGBValues = (function() {

    var _hex2dec = function(v) {
        return parseInt(v, 16)
    };

    var _splitHEX = function(hex) {
        var c;
        if (hex.length === 4) {
            c = (hex.replace('#','')).split('');
            return {
                red: _hex2dec((c[0] + c[0])),
                green: _hex2dec((c[1] + c[1])),
                blue: _hex2dec((c[2] + c[2]))
            };
        } else {
             return {
                red: _hex2dec(hex.slice(1,3)),
                green: _hex2dec(hex.slice(3,5)),
                blue: _hex2dec(hex.slice(5))
            };
        }
    };

    var _splitRGB = function(rgb) {
        var c = (rgb.slice(rgb.indexOf('(')+1, rgb.indexOf(')'))).split(',');
        var flag = false, obj;
        c = c.map(function(n,i) {
            return (i !== 3) ? parseInt(n, 10) : flag = true, parseFloat(n);
        });
        obj = {
            red: c[0],
            green: c[1],
            blue: c[2]
        };
        if (flag) obj.a = c[3];
        return obj;
    };

    var color = function(col) {
        var slc = col.slice(0,1);
        if (slc === '#') {
            return _splitHEX(col);
        } else if (slc.toLowerCase() === 'r') {
            return _splitRGB(col);
        } else {
            console.log('!Ooops! RGBvalues.color('+col+') : HEX, RGB, or RGBa strings only');
        }
    };

    return {
        color: color
    };
}());

// throttle function taken from: https://wiki.selfhtml.org/wiki/JavaScript/Tutorials/Debounce_und_Throttle
Function.prototype.throttle = function(minimumDistance) {
   let timeout,
       lastCalled = 0,
       throttledFunction = this;

   function throttleCore() {
      let context = this;

      function callThrottledFunction(args) {
         lastCalled = Date.now();
         throttledFunction.apply(context, args);
      }
      // Wartezeit bis zum nächsten Aufruf bestimmen
      let timeToNextCall = minimumDistance - (Date.now() - lastCalled);
      // Egal was kommt, einen noch offenen alten Call löschen
      cancelTimer();
      // Aufruf direkt durchführen oder um offene Wartezeit verzögern
      if (timeToNextCall < 0) {
         callThrottledFunction(arguments, 0);
      } else {
         timeout = setTimeout(callThrottledFunction, timeToNextCall, arguments);
      }
   }
   function cancelTimer() {
      if (timeout) {
         clearTimeout(timeout);
         timeout = undefined;
      }
   }
   // Aufsperre aufheben und gepeicherte Rest-Aufrufe löschen
   throttleCore.reset = function() {
      cancelTimer();
      lastCalled = 0;
   }
   return throttleCore;
};


document.addEventListener('DOMContentLoaded', function() {
  var fayeClient = new Faye.Client('http://192.168.20.93:3000/faye');

  for (let ledNum = 0; ledNum < 18; ledNum++) {
    var ledElt = document.getElementById('led' + ledNum);
    ledElt.value = '#ffffff';
    let ledListener = function (event) {
      var rgbValues = RGBValues.color(event.target.value);
      fayeClient.publish('/arduino/led/set', {leds: [{pin: ledNum, rgb: rgbValues}]});
    };
    let debouncedLedListener = ledListener.throttle(100);
    ledElt.addEventListener('change', debouncedLedListener);
    ledElt.addEventListener('input', debouncedLedListener);
  }

  document.getElementById('ledDefault1').addEventListener('click', function () {
    const ledDefaults = [
        '#770000', '#770000', '#000000', '#000000', '#000000', '#000000', '#000000', '#770000', '#770000',
        '#999999', '#999999', '#000000', '#000000', '#000000', '#000000', '#000000', '#999999', '#999999'];
    var leds = [];
    for (let ledNum = 0; ledNum < 18; ledNum++) {
      var ledElt = document.getElementById('led' + ledNum);
      ledElt.value = ledDefaults[ledNum];

      var rgbValues = RGBValues.color(ledDefaults[ledNum]);
      leds.push({pin: ledNum, rgb: rgbValues});
    }

    fayeClient.publish('/arduino/led/set', {leds});
  });
  document.getElementById('ledDefault2').addEventListener('click', function () {
    const ledDefaults = [
        '#FFFF7F', '#FFFF7F', '#FFFFFF', '#FFFF7F', '#FFFFFF', '#FFFF7F', '#FFFFFF', '#FFFF7F', '#FFFF7F'];
    var leds = [];
    for (let ledNum = 0; ledNum < 9; ledNum++) {
      var ledElt = document.getElementById('led' + (ledNum + 9));
      ledElt.value = ledDefaults[ledNum];

      var rgbValues = RGBValues.color(ledDefaults[ledNum]);
      leds.push({pin: ledNum + 9, rgb: rgbValues});
    }

    fayeClient.publish('/arduino/led/set', {leds});
  });
  document.getElementById('ledDefault3').addEventListener('click', function () {
    const ledDefaults = [
        '#000000', '#000000', '#000000', '#000000', '#000000', '#000000', '#000000', '#000000', '#000000',
        '#000000', '#000000', '#000000', '#000000', '#000000', '#000000', '#000000', '#000000', '#000000'];
    var leds = [];
    for (let ledNum = 0; ledNum < 18; ledNum++) {
      var ledElt = document.getElementById('led' + ledNum);
      ledElt.value = ledDefaults[ledNum];

      var rgbValues = RGBValues.color(ledDefaults[ledNum]);
      leds.push({pin: ledNum, rgb: rgbValues});
    }

    fayeClient.publish('/arduino/led/set', {leds});
  });
});
