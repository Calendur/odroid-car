"use strict";
// jslint assume node option defaults:
/*global Buffer: false, clearInterval: false, clearTimeout: false, console: false, exports: false, global: false, module: false, process: false, querystring: false, require: false, setInterval: false, setTimeout: false, __filename: false, __dirname: false */

var winston = require('winston');

winston.configure({
  level: 'info',
  transports: [
    new winston.transports.Console()
  ]
});

var express = {
  main: require('express'),
  bodyParser: require('body-parser'),
  cookieParser: require('cookie-parser'),
  errorHandler: require('errorhandler'),
  morgan: require('morgan'),
};
var http = require('http');
var app = express.main();
var server = http.createServer(app);
var path = require('path');
var fs = require('fs');
var faye = require('faye');

// all environments
app.set('port', process.env.PORT || 3000);
app.use(express.morgan('dev'));
app.use(express.cookieParser('eudhs7a62a7d'));
app.use(express.bodyParser.json());

// development only
if ('development' === app.get('env')) {
  app.use(express.errorHandler());
}

app.use(express.main.static(path.join(__dirname, 'public')));

http.globalAgent.maxSockets = 100; // set this high, if you use httpClient or request anywhere (defaults to 5)

server.listen(app.get('port'), function () {
  winston.log('info', 'Express server listening on port ' + app.get('port'));
});


var bayeux = new faye.NodeAdapter({ mount: '/faye', timeout: 45 });
var fayeClient = bayeux.getClient();
bayeux.on('subscribe', function (clientId, channel) {
  winston.log('info', '[  SUBSCRIBE] ' + clientId + ' -> ' + channel);
});
bayeux.on('unsubscribe', function (clientId, channel) {
  winston.log('info', '[UNSUBSCRIBE] ' + clientId + ' -> ' + channel);
});
bayeux.on('disconnect', function (clientId) {
  winston.log('info', '[ DISCONNECT] ' + clientId);
});
bayeux.attach(server);

exports.fayeClient = fayeClient;
fayeClient.subscribe('/common/pageload', function (msg) {
  //  winston.log('trace', 'Client loaded page ' + JSON.stringify({msg: msg, name: name, timestamp: timestamp}));
  winston.log('info', 'Client loaded page ' + JSON.stringify(msg));
});

require('./modules/ArduinoAdapter.js').init(fayeClient, { port: '/dev/ttyUSB0' });
require('./modules/OpenCVAdapter.js').init(fayeClient, {  });

