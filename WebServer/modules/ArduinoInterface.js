var ArduinoFirmata = require('arduino-firmata');
var events = require('eventemitter2');
var arduino = new ArduinoFirmata();

var adcValues = [];

class ArduinoInterface extends events.EventEmitter2 {
  constructor(port = "/dev/ttyUSB0") {
    super();
    arduino.connect(port, {baudRate: 115200});
    
    var self = this;
	  arduino.once('boardReady', function(){
	    self.ready = true;
        console.log("Board Ready.");
        self.emit('boardReady');
	  });

    arduino.on('analogChange', function (opt) {
      self.emit('analogChange', opt);
    });
/*
	  arduino.on('analogChange', function(opt) {
if (opt.pin == 6) {
      adcValues.push(opt.value);
      if (adcValues.length > 5) {adcValues.shift();}
      var mean = 0;
      for (var idx = 0;idx < adcValues.length; idx++) {mean += adcValues[idx];}
      mean /= 5;
      console.log("Analog Change Pin " + opt.pin + " = " + opt.value + " ; Mean = " + mean);
}
    });
	  arduino.on('digitalChange', function(opt) {
      console.log("Digital Change Pin " + opt.pin + " = " + opt.value);
    });
*/

	  arduino.on('sysex', function(e){
	    if (e.command == 1) {
        // raw IMU measurement message
		    self._parseGyroMessage(e.data);
	    } else if (e.command == 2) {
        // odometry message
		    self._parseOdometryMessage(e.data);
	    } else if (e.command == 3) {
        // IMU Calibration message
		    self._parseIMUCalibrationMessage(e.data);
	    } else if (e.command == 113) { // 113 = 71 = String?
		    console.log("command : " + e.command + ": " + self._convertSysExDataToString(e.data));
	    }
	  });
  }

  _convertSysExDataToString(data) {
    var str = "";
    for (var i = 0; i < data.length; i+=2) {
      str += String.fromCharCode(data[i]);
    }
    return str;
  }
  _parseIMUCalibrationMessage(buf) {
    //console.log("Raw Buffer: " + JSON.stringify(buf));
    for (var i = 0; i < buf.length; i += 2) {
      buf[i / 2] = (buf[i]) + ((buf[i+1] & 0x01) << 7);
    }
    buf = Buffer.from(buf.slice(0, buf.length / 2));
    //console.log("Buffer: " + JSON.stringify(buf));
    const accXBias = buf.readFloatLE(0);
    const accYBias = buf.readFloatLE(4);
    const accZBias = buf.readFloatLE(8);
    const gyroXBias = buf.readFloatLE(12);
    const gyroYBias = buf.readFloatLE(16);
    const gyroZBias = buf.readFloatLE(20);

    console.log("IMU Calibration: " + JSON.stringify({accXBias, accYBias, accZBias, gyroXBias, gyroYBias, gyroZBias}));
    //this.emit("imuCalibration", {timeMs, posX, posY, heading});
  }
  _parseOdometryMessage(buf) {
    //console.log("Raw Buffer: " + JSON.stringify(buf));
    for (var i = 0; i < buf.length; i += 2) {
      buf[i / 2] = (buf[i]) + ((buf[i+1] & 0x01) << 7);
    }
    buf = Buffer.from(buf.slice(0, buf.length / 2));
    //console.log("Buffer: " + JSON.stringify(buf));
    const timeMs = buf.readUInt16LE(0);
    const posX = buf.readFloatLE(2);
    const posY = buf.readFloatLE(6);
    const heading = buf.readFloatLE(10);

    this.emit("odometry", {timeMs, posX, posY, heading});
  }
  _parseGyroMessage(buf) {
    //console.log("Raw Buffer: " + JSON.stringify(buf));
    for (var i = 0; i < buf.length; i += 2) {
      buf[i / 2] = (buf[i]) + ((buf[i+1] & 0x01) << 7);
    }
    buf = Buffer.from(buf.slice(0, buf.length / 2));
    //console.log("Buffer: " + JSON.stringify(buf));
    const timeMs = buf.readUInt16LE(0);
    const accX = buf.readFloatLE(2);
    const accY = buf.readFloatLE(6);
    const accZ = buf.readFloatLE(10);
    const gyroX = buf.readFloatLE(14);
    const gyroY = buf.readFloatLE(18);
    const gyroZ = buf.readFloatLE(22);
    const magX = buf.readFloatLE(26);
    const magY = buf.readFloatLE(30);
    const magZ = buf.readFloatLE(34);

    this.emit("gyro", {timeMs, accX, accY, accZ, gyroX, gyroY, gyroZ, magX, magY, magZ});
  }

  getPowerButton() {
    return arduino.digitalRead(0);
  }
  getAccuVoltage() {
    return arduino.analogRead(6);
  }

  getSonar1() {
    return arduino.analogRead(2);
  }
  getSonar2() {
    return arduino.analogRead(3);
  }

  /**
   * @param[in] speed 0 to 256; 0 = stop
   * @param[in] direction 1 = forward, 0 = stop, -1 = backward
   */
  setLeftMotor(speed, direction) {
    arduino.analogWrite(1, 256 - speed * direction);
  }
  /**
   * @param[in] speed 0 to 256; 0 = stop
   * @param[in] direction 1 = forward, 0 = stop, -1 = backward
   */
  setRightMotor(speed, direction) {
    arduino.analogWrite(0, 256 - speed * direction);
  }

  setPowerLed(state) {
    arduino.digitalWrite(0, (state) ? 1 : 0);
  }
  setOdodmetryEnabled(state) {
    arduino.digitalWrite(8, (state) ? 1 : 0);
  }
  setIMURawDataReportingEnabled(state) {
    arduino.digitalWrite(16, (state) ? 1 : 0);
  }
  resetOdometryToZero() {
    arduino.digitalWrite(24, 1);
  }
  startIMUCalibration() {
    /* 32 = 1; 33 = 2; 34 = 4; 35 = 8; 36 = 16; 37 = 32; 38 = 64*/
    arduino.digitalWrite(37, 1);
  }

  setRGBLed(number, rgb) {
    if (rgb.hasOwnProperty("write")) {
      arduino.extendedAnalogWrite(number, (rgb.red >>> 1) | ((rgb.green >>> 1) << 7) | ((rgb.blue >>> 1) << 14) | 1 << 16);
    } else {
      arduino.extendedAnalogWrite(number, (rgb.red >>> 1) | ((rgb.green >>> 1) << 7) | ((rgb.blue >>> 1) << 14));
    }
  }

  setLed(number, red, green, blue) {
    arduino.extendedAnalogWrite(number, (red >>> 1) | ((green >>> 1) << 7) | ((blue >>> 1) << 14));
  }
}

module.exports = ArduinoInterface;
