var ArduinoInterface = require('./ArduinoInterface.js');
const fs = require('fs');
const Stream = require('stream');
const { createGzip } = require('zlib');
const process = require('process');
const readableStream = new Stream.Readable({read() {}});
const gzip = createGzip();

var csvFile = fs.createWriteStream('arduinoLogs.csv.gz', {flags: 'w'});

Stream.pipeline(readableStream, gzip, csvFile, (err) => {
  if (err) {
    console.error('An error occurred:', err);
  }
});

process.on('exit', function () {
  console.log("ON EXIT.");
  readableStream.push(null);
});
process.on('SIGINT', function () {
  console.log("ON SIGINT.");
  readableStream.push(null);
  setTimeout(function () {
    process.exit(1);
  }, 2000);
});

var csvData = {};

module.exports.init = function (fayeClient, options) {
  var arduino = new ArduinoInterface(options.port);

  arduino.once('boardReady', function () {
    console.log("Board Ready 2!");
    fayeClient.publish('/arduino/boardReady', {});

    fayeClient.subscribe('/arduino/led/set', function (msg) {
      console.log("arduino set led " + JSON.stringify(msg));
      for (var idx = 0; idx < msg.leds.length; idx++) {
        var rgb = msg.leds[idx].rgb;
        if (idx == msg.leds.length -1) {
          rgb.write = true;
        }
        arduino.setRGBLed(msg.leds[idx].pin, rgb);
      }
    });

    arduino.setPowerLed(true);

    arduino.on('analogChange', function(e){
      switch (e.pin) {
        case 2:
          csvData.sonar1 = e.value;
          fayeClient.publish('/arduino/sonar', {sensor: 2, distance: e.value});
          break;
        case 3:
          csvData.sonar2 = e.value;
          fayeClient.publish('/arduino/sonar', {sensor: 3, distance: e.value});
          break;
        case 6:
          csvData.accuVoltage = e.value;
          fayeClient.publish('/arduino/accuVoltage', {value: e.value});
          break;
      }
    });
    /*setInterval(function () {
      console.log("Button? " + arduino.getPowerButton());
      console.log("Battery: " + arduino.getAccuVoltage());
      console.log("Sonar: " + arduino.getSonar1() + ", " + arduino.getSonar2());
    }, 500);*/

    var outputCounter = 0;
    arduino.on('gyro', function (obj) {
      var dataArray = [Date.now(), obj.timeMs, obj.accX, obj.accY, obj.accZ, obj.gyroX, obj.gyroY, obj.gyroZ, obj.magX, obj.magY, obj.magZ, csvData.leftMotorSpeed, csvData.leftMotorDirection, csvData.rightMotorSpeed, csvData.rightMotorDirection, csvData.sonar1, csvData.sonar2, csvData.accuVoltage];
      readableStream.push(dataArray.join(';') + "\n");
      if (outputCounter++ > 20) {
        console.log("" + Date.now() + ", " + obj.timeMs + ", Gyro: " + obj.gyroX + ", " +  obj.gyroY + ", " +  obj.gyroZ + ", Acc: " + obj.accX + ", " +  obj.accY + ", " +  obj.accZ);
        outputCounter = 0;
      }
    });

    var outputCounter1 = 1;
    arduino.on('odometry', function (obj) {
      if (outputCounter1++ > 10) {
        console.log("" + Date.now() + ", " + obj.timeMs + ", X: " + obj.posX + ", Y: " +  obj.posY + ", Heading: " +  obj.heading);
        outputCounter1 = 0;
      }
    });

    fayeClient.subscribe('/arduino/motor/set', function (msg) {
      console.log("arduino set motors " + JSON.stringify(msg));
      if (msg.hasOwnProperty('left')) {
        csvData.leftMotorSpeed = msg.left.speed;
        csvData.leftMotorDirection = msg.left.direction;
        arduino.setLeftMotor(msg.left.speed, msg.left.direction);
        setTimeout(function () {
          csvData.leftMotorSpeed = 0;
          csvData.leftMotorDirection = 0;
          arduino.setLeftMotor(0, 0);
        }, msg.left.time);
      }
      if (msg.hasOwnProperty('right')) {
        csvData.rightMotorSpeed = msg.right.speed;
        csvData.rightMotorDirection = msg.right.direction;
        arduino.setRightMotor(msg.right.speed, msg.right.direction);
        setTimeout(function () {
          csvData.rightMotorSpeed = 0;
          csvData.rightMotorDirection = 0;
          arduino.setRightMotor(0, 0);
        }, msg.right.time);
      }
    });
    fayeClient.subscribe('/arduino/odometry/zero', function (msg) {
      arduino.resetOdometryToZero();
    });
    fayeClient.subscribe('/arduino/imu/calibrate', function (msg) {
      arduino.startIMUCalibration();
    });
  });
}
