const fs = require('fs');
const dgram = require('dgram');
const socket = dgram.createSocket('udp4');

const opencvPortPython = 3220; // C=3 V=22
const opencvPortLocal = 3221;

module.exports.init = function (fayeClient, options) {
  socket.bind(opencvPortLocal); 
  socket.on('error', (err) => {
    console.log("socket error:\n${err.stack}");
    socket.close();
  });

  socket.on('message', (msg, rinfo) => {
    console.log("server got: ${msg} from ${rinfo.address}:${rinfo.port}");
  });

  socket.on('listening', () => {
    const address = socket.address();
    console.log("socket listening ${address.address}:${address.port}");
  });


  fayeClient.subscribe('/opencv/distanceimage/get', function (msg) {
    var message = JSON.stringify({getDistanceImage: true});
    socket.send(message, opencvPortPython, '127.0.0.1', (err) => {
    });
  });
};

