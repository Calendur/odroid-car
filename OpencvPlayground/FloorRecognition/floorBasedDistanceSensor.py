#!/usr/bin/env python3

# source is https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_geometric_transformations/py_geometric_transformations.html

# algorithm:
# - open UDP socket
# - wait for UDP message
# - make image from webcam
# - save the image and send a UDP message back

import numpy as np
import cv2
import socket
from datetime import datetime


pts1 = np.float32([[640-16, 480-328], [640-93, 480-281], [640-515, 480-260], [640-608, 480-302]])
# target coordinates = image of size 200x200
# camera-center at 100x0
# 1 pixel = 1 cm
scaling = 10
center = 100
pts2 = np.float32([[center + 245/scaling, 712/scaling], [center + 245/scaling, 1007/scaling], [center - 200/scaling, 1045/scaling], [center - 200/scaling, 750/scaling]])
transformationMatrix = cv2.getPerspectiveTransform(pts1,pts2)

def transformPoint(x, y):
    point = np.matmul(transformationMatrix, [x, y, 1])
    #print("transformed point (" + str(x) +  ", " + str(y) + ") => (" + str(point[0] / point[2]) + ", " + str(point[1] / point[2]) + "); RAW = " + str(point))
    return [point[0] / point[2] , point[1] / point[2]]

def detectFloor(img, outputImage):
    allowedColorDistance = 30
    polyLine = []
    polyLine.append(transformPoint(0, 0));

    for x in range(0, img.shape[1], 15):
        lastFloorColors = np.zeros((5, 3)) # 5 colors, with 3 values each
        currentIndex = 1
        for i in range(lastFloorColors.shape[0]):
            lastFloorColors[i] = img[1, x]

        # allow 1 pixel to not match to get a more robust result
        lastPointOk = 1
        for y in range(2, img.shape[0], 5):
            # create a mean color over the last 5 pixels, and compare to them (to cope with color gradients)
            distance = np.linalg.norm(img[y, x] - np.mean(lastFloorColors, axis=0))
            if (distance > allowedColorDistance):
                if (lastPointOk == 0):
                    polyLine.append(transformPoint(x, y + 2));
                    break
                else:
                    lastPointOk = 0
            else:
                lastPointOk = 1

            lastFloorColors[currentIndex] = img[y, x]
            currentIndex = (currentIndex + 1) % lastFloorColors.shape[0]

    polyLine.append(transformPoint(img.shape[1], 0));
    polyGone = np.array([polyLine], dtype=np.int32)
    polyLine = polyLine[1:-1]
    polyLine = np.array([polyLine], dtype=np.int32)

    cv2.fillPoly(outputImage, polyGone, (255))
    cv2.polylines(outputImage, polyLine, False, (0))

#img = cv2.imread('my_photo-9.jpg')
#outputImage = np.full(shape=[200, 200, 1], fill_value=128, dtype=np.uint8)
#detectFloor(img, outputImage);
#dst = cv2.warpPerspective(img, transformationMatrix, (200, 200))

#cv2.imshow('outputImage', outputImage)
#cv2.imshow('transformed image', dst)

webcam = cv2.VideoCapture(0)
webcam.set(cv2.CAP_PROP_FPS, 5)

UDP_IP = "0.0.0.0"
UDP_PORT = 3220 #C3 V22
 
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))
sock.setblocking(0)

while True:
    return_value, webcamimg = webcam.read()

    try:
        data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
        currentTime = datetime.now().strftime("%H:%M:%S.%f")
        print("%s: received message:" % currentTime)
        outputImage = np.full(shape=[200, 200, 1], fill_value=128, dtype=np.uint8)
        detectFloor(webcamimg, outputImage);
        currentTime = datetime.now().strftime("%H:%M:%S.%f")
        print("%s: Algorithm done" % currentTime)
        # car dimensions: 28cm (length) x 21cm (wide)
        cv2.rectangle(outputImage, (90, 0), (110, 28), (200), -1)
        cv2.imwrite('webcamImage.png', webcamimg)
        cv2.imwrite('distanceSensor.png', outputImage)
        currentTime = datetime.now().strftime("%H:%M:%S.%f")
        print("%s: Image Written" % currentTime)
    except socket.error:
        pass
