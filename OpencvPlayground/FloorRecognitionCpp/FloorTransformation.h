#pragma once

#include <opencv2/opencv.hpp>

class FloorTransformation
{
  public:
    static void getTransformationMatrix();
    static cv::Point transformPoint(int x, int y);

  protected:
    static cv::Mat transformationMatrix;
};

