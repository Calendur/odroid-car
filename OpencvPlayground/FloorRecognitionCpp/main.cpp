#include <opencv2/opencv.hpp>
#include <iostream>
#include <cstdint>
#include <cmath>

#include "MeanColorFilter.h"
#include "FloorTransformation.h"

cv::Mat FloorTransformation::transformationMatrix;

void detectFloor(cv::Mat const &image, cv::Mat &outputImage)
{
    std::vector<cv::Point> polyLine;
    polyLine.push_back(FloorTransformation::transformPoint(0, 0));
    for (int x = 0; x < image.cols; x += 15)
    {
        MeanColorFilter floorColorFilter(image.at<cv::Vec3b>(1, x));

        // allow 1 pixel to not match to get a more robust result
        bool lastPointOk = true;

        for (int y = 2; y < image.rows; y += 5)
        {
            if (floorColorFilter.checkPixel(image.at<cv::Vec3b>(y, x)))
            {
                lastPointOk = true;
            }
            else
            {
                if (!lastPointOk)
                {
                    //polyLine.push_back(cv::Point(x, y + 2));
                    polyLine.push_back(FloorTransformation::transformPoint(x, y + 2));
                    break;
                }
                else
                {
                    lastPointOk = false;
                }
            }

            floorColorFilter.addPixel(image.at<cv::Vec3b>(y, x));
        }
    }
    polyLine.push_back(FloorTransformation::transformPoint(image.cols, 0));

    {
        cv::Point const * polyLinePoints = polyLine.data();
        int const polyLinePointAmount = polyLine.size();
        cv::fillPoly(outputImage, &polyLinePoints, &polyLinePointAmount, 1, cv::Scalar(255));
    }

    if (polyLine.size() > 2)
    {
        polyLine.pop_back();
        cv::Point const * polyLinePoints = &polyLine.at(1);
        int const polyLinePointAmount = polyLine.size() - 1;
        cv::polylines(outputImage, &polyLinePoints, &polyLinePointAmount, 1, false, cv::Scalar(0));
    }
}

int main(int argc, char** argv)
{
    FloorTransformation::getTransformationMatrix();
    
    bool const useImage = false;

    if (useImage)
    {
        cv::Mat const image = cv::imread("testImage.png", cv::IMREAD_COLOR);
        cv::Mat outputImage(200, 200, CV_8UC1, cv::Scalar(128));

        auto const startTime = std::chrono::steady_clock::now();
        detectFloor(image, outputImage);
        auto const endTime = std::chrono::steady_clock::now();
        std::cout << "Algorithm used: " << std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count() << " ms" << std::endl;

        cv::imshow("input", image);
        cv::imshow("output", outputImage);
        
        while (cv::waitKey(100) != 27)
        {
            ;
        }
    }
    else
    {
        cv::VideoCapture cap(0);
        cv::Mat image;
        cv::Mat outputImage(200, 200, CV_8UC1, cv::Scalar(128));

        int frameCount = 0;
        while (true)
        {
            if (cap.isOpened())
            {
                cap.read(image);
                frameCount++;
            
                if (frameCount % 5 == 1)
                {
                    auto const startTime = std::chrono::steady_clock::now();
                    cv::rectangle(outputImage, cv::Point(0, 0), cv::Point(200, 200), cv::Scalar(128), cv::FILLED);
                    detectFloor(image, outputImage);
                    auto const endTime = std::chrono::steady_clock::now();
                    std::cout << "Algorithm used: " << std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count() << " ms" << std::endl;
                
                    cv::imshow("image", image);
                    cv::imshow("outputImage", outputImage);
                }
            }
            if (cv::waitKey(2) == 27)
            {
                break;
            }
        }
    }
}
