#include "MeanColorFilter.h"

#include <cmath>

MeanColorFilter::MeanColorFilter(cv::Vec3b const &pixelRgb)
{
    for (auto &pixel : m_pixels) {
        pixel = pixelRgb;
    }
    m_mean = pixelRgb;
}

void MeanColorFilter::addPixel(cv::Vec3b const &pixelRgb)
{
    for (int i = 0; i < 3; i++) {
        m_mean[i] += (static_cast<float>(pixelRgb[i]) - m_pixels[m_index][i]) / pixelCount;
    }
    m_pixels[m_index] = pixelRgb;
    m_index = (m_index + 1) % pixelCount;
}

bool MeanColorFilter::checkPixel(cv::Vec3b const &pixelRgb)
{
    float distance = 0.0;
    
    for (int i = 0; i < 3; i++) {
        distance += std::abs(pixelRgb[i] - m_mean[i]);
    }
    return (distance <= allowedColorDistance);
}

