#pragma once

#include <array>
#include <opencv2/opencv.hpp>

class MeanColorFilter
{
  public:
    MeanColorFilter(cv::Vec3b const &pixelRgb);

    void addPixel(cv::Vec3b const &pixelRgb);
    bool checkPixel(cv::Vec3b const &pixelRgb);

  protected:
    static int constexpr pixelCount = 5;
    static int constexpr allowedColorDistance = 30;
    std::array<cv::Vec3b, pixelCount> m_pixels;
    cv::Vec3f m_mean;
    int m_index = 1;
};

