#include "FloorTransformation.h"

void FloorTransformation::getTransformationMatrix()
{
    cv::Point2f srcPoints[] = {
        cv::Point2f(640 -  16, 480 - 328),
        cv::Point2f(640 -  93, 480 - 281),
        cv::Point2f(640 - 515, 480 - 260),
        cv::Point2f(640 - 608, 480 - 302)};
    // target coordinates = image of size 200x200
    // camera-center at 100x0
    // 1 pixel = 1 cm
    float const scaling = 10.0;
    int const center = 100;
    cv::Point2f dstPoints[] = {
        cv::Point2f(center + 245 / scaling,  712 / scaling),
        cv::Point2f(center + 245 / scaling, 1007 / scaling),
        cv::Point2f(center - 200 / scaling, 1045 / scaling),
        cv::Point2f(center - 200 / scaling,  750 / scaling)};
    transformationMatrix = cv::getPerspectiveTransform(srcPoints, dstPoints);
}
cv::Point FloorTransformation::transformPoint(int x, int y)
{
    /*
     ( a b c )   (x)   (ax + by + c)
     ( d e f ) * (y) = (dx + ey + f)
     ( g h i )   (1)   (gx + hy + i)
     */
    if (0) {
        std::cout << "CalculationX = " << 
            transformationMatrix.at<double>(0, 0) << " * " << x << " + " << 
            transformationMatrix.at<double>(0, 1) << " * " << y << " + " << 
            transformationMatrix.at<double>(0, 2) << std::endl;
        std::cout << "CalculationY = " << 
            transformationMatrix.at<double>(1, 0) << " * " << x << " + " << 
            transformationMatrix.at<double>(1, 1) << " * " << y << " + " << 
            transformationMatrix.at<double>(1, 2) << std::endl;
        std::cout << "CalculationT = " << 
            transformationMatrix.at<double>(2, 0) << " * " << x << " + " << 
            transformationMatrix.at<double>(2, 1) << " * " << y << " + " << 
            transformationMatrix.at<double>(2, 2) << std::endl;
    }
    float const t = transformationMatrix.at<double>(2, 0) * x + transformationMatrix.at<double>(2, 1) * y + transformationMatrix.at<double>(2, 2);
    return cv::Point((transformationMatrix.at<double>(0, 0) * x + transformationMatrix.at<double>(0, 1) * y + transformationMatrix.at<double>(0, 2)) / t, (transformationMatrix.at<double>(1, 0) * x + transformationMatrix.at<double>(1, 1) * y + transformationMatrix.at<double>(1, 2)) / t);
}

