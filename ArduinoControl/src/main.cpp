//#define FIRMATA_DEBUG 1

#include <Arduino.h>
#include <Firmata.h>
#include <Wire.h>

#include "KalmanFilter.h"
#include "MotorControl.h"
#include <AsyncSonarLib.h>
#include <FastLED.h>
#include <MPU9250.h>

static uint8_t const WS2813_NUM_LEDS = 18;
static uint8_t const WS2813_LED_PIN = 4;
CRGB leds[WS2813_NUM_LEDS];
MPU9250 IMU(Wire, 0x68);
KalmanFilter kalmanRoll;
KalmanFilter kalmanPitch;
AsyncSonar sonar1(7);
AsyncSonar sonar2(8);

uint8_t const odroidButtonLedPin = 2;
MotorControl motor1(5, 6);
MotorControl motor2(9, 10);

void analogWriteCallback(byte pin, int value) {
#ifdef FIRMATA_DEBUG
  char msg[20];
  sprintf(msg, "A%d=%d", pin, value);
  Firmata.sendString(msg);
#endif
  // PWM values from 0 to 255
  if (pin == 0) { // motor 1
    motor1.set(value);
    Firmata.setPinState(pin, value);
  } else if (pin == 1) { // motor 2
    motor2.set(value);
    Firmata.setPinState(pin, value);
  }
}

void digitalWriteCallback(byte pin, int value) {
#ifdef FIRMATA_DEBUG
  char msg[20];
  sprintf(msg, "D%d=%d", pin, value);
  Firmata.sendString(msg);
#endif
  if (pin == 0) {
    pinMode(odroidButtonLedPin, (value == 1) ? INPUT_PULLUP : INPUT);
    Firmata.setPinState(pin, value);
  }
}

/*==============================================================================
 * SYSEX-BASED commands
 *============================================================================*/
void sysexCallback(byte command, byte argc, byte *argv) {
  switch (command) {
  case EXTENDED_ANALOG:
    if (argc <= 2)
      return;
    byte const pin = argv[0];
    if (pin >= 0 && pin < 18 && argc > 3) {
      // NeoPixel RGB Values each byte is one color code (with 7 bits)
      byte const red = argv[1] << 1;
      byte const green = argv[2] << 1;
      byte const blue = argv[3] << 1;
#ifdef FIRMATA_DEBUG
      char msg[20];
      sprintf(msg, "A%d=%d,%d,%d", pin, red, green, blue);
      Firmata.sendString(msg);
#endif
      leds[pin].setRGB(red, green, blue);
      if (argc > 4) {
        FastLED.show();
      }
    }
    break;
  }
}

void setup() {
  // pinMode(A3, INPUT_PULLUP);
  pinMode(odroidButtonLedPin, INPUT);

  // red and green need to be changed...
  FastLED.addLeds<WS2813, WS2813_LED_PIN, GRB>(leds, WS2813_NUM_LEDS);

  if (0) { // some light show upon initialization
    for (uint8_t i = 0; i < WS2813_NUM_LEDS; i++) {
      if (i >= 2)
        leds[i - 2].setRGB(0, 0, 0);
      if (i >= 1)
        leds[i - 1].setRGB(30, 30, 30);
      leds[i + 0].setRGB(100, 100, 100);
      if (i + 1 < WS2813_NUM_LEDS)
        leds[i + 1].setRGB(30, 30, 30);

      FastLED.show();
      delay(100);
    }
    for (uint8_t i = 0; i < WS2813_NUM_LEDS; i++) {
      leds[i].setRGB(0, 0, 0);
    }
    FastLED.show();
  }

  Firmata.setFirmwareVersion(FIRMATA_FIRMWARE_MAJOR_VERSION,
                             FIRMATA_FIRMWARE_MINOR_VERSION);
  Firmata.attach(ANALOG_MESSAGE, analogWriteCallback);
  Firmata.attach(DIGITAL_MESSAGE, digitalWriteCallback);
  Firmata.attach(START_SYSEX, sysexCallback);
  Firmata.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for ATmega32u4-based boards
      // and Arduino 101
  }

  // start communication with IMU
  IMU.begin();
  // setting the accelerometer full scale range to +/-2G
  IMU.setAccelRange(MPU9250::ACCEL_RANGE_2G);
  // setting the gyroscope full scale range to +/-500 deg/s
  IMU.setGyroRange(MPU9250::GYRO_RANGE_250DPS);
  // setting DLPF bandwidth to 20 Hz
  IMU.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_20HZ);
  // setting SRD to 19 for a 50 Hz update rate
  IMU.setSrd(19);

  int const temp = IMU.getTemperature_C();
  sonar1.SetTemperatureCorrection(temp);
  sonar2.SetTemperatureCorrection(temp);
}

unsigned long lastReportTime = 0;
unsigned long const reportTimeMillis = 500;
unsigned long lastSonarTime = 0;
unsigned long const sonarTimeMillis = 100;
unsigned long lastIMUMeasurementTime = 0;
unsigned long const IMUMeasurementTimeMillis = 20;
void loop() {
  while (Firmata.available()) {
    Firmata.processInput();
  }
  motor1.update();
  motor2.update();

  unsigned long const currentTime = millis();
  if (currentTime - lastReportTime >= reportTimeMillis) {
    lastReportTime = currentTime;
    {
      // battery voltage uses a voltage divider to get only half the voltage
      // and we wan't to convert the reading into a voltage signal (as mV)
      /*
       * Since the operating and thus ADC reference voltage is not
       * 5 volts, we need to make some measurements:
       *   Voltage | ADC Measurement
       *   8,7       1008
       *   8,3        958
       *   7,91       915
       *   7,55       872,5
       *   6,99       806
       *   6,01       695
       */
      int const batteryVoltageInMv = 8.6 * analogRead(A7) + 40;
      Firmata.sendAnalog(6, batteryVoltageInMv);
    }
    {
      // pin 2 == button to shut down the odroid
      Firmata.sendDigitalPort(0, digitalRead(2));
    }
    // for sendAnalog: pin numbers shall be lower than 15, because
    // otherwise it uses another message and not the
    // standard analog message

    // this could maybe help to light up all the LEDs, sometimes it makes
    // problems when showing a lot of LEDs, then maybe the last commands are not
    // executed
    FastLED.show();
  }

  if (currentTime - lastSonarTime >= sonarTimeMillis) {
    lastSonarTime = currentTime;

    switch (sonar1.GetStatus()) {
    case AsyncSonarStatus::IDLE:
      sonar1.Start();
      break;
    case AsyncSonarStatus::FINISHED:
      Firmata.sendAnalog(2, sonar1.GetFilteredMM());
    default:
      sonar1.Update();
      break;
    }
    switch (sonar2.GetStatus()) {
    case AsyncSonarStatus::IDLE:
      sonar2.Start();
      break;
    case AsyncSonarStatus::FINISHED:
      Firmata.sendAnalog(3, sonar2.GetFilteredMM());
    default:
      sonar2.Update();
      break;
    }
  }
  if (currentTime - lastIMUMeasurementTime >= IMUMeasurementTimeMillis) {
    lastIMUMeasurementTime = currentTime;
    IMU.readSensor();

    union uIMUMessage {
      struct {
        float accX;
        float accY;
        float accZ;
        float gyroX;
        float gyroY;
        float gyroZ;
        float magX;
        float magY;
        float magZ;
      } measurement;
      byte data[12 * sizeof(float)];
    } imuMsg;
    imuMsg.measurement.accX = IMU.getAccelX_mss();
    imuMsg.measurement.accY = IMU.getAccelY_mss();
    imuMsg.measurement.accZ = IMU.getAccelZ_mss();
    imuMsg.measurement.gyroX = IMU.getGyroX_rads() * RAD_TO_DEG;
    imuMsg.measurement.gyroY = IMU.getGyroY_rads() * RAD_TO_DEG;
    imuMsg.measurement.gyroZ = IMU.getGyroZ_rads() * RAD_TO_DEG;
    imuMsg.measurement.magX = IMU.getMagX_uT();
    imuMsg.measurement.magY = IMU.getMagY_uT();
    imuMsg.measurement.magZ = IMU.getMagZ_uT();
    double const roll =
        atan(imuMsg.measurement.accY /
             sqrt(imuMsg.measurement.accX * imuMsg.measurement.accX +
                  imuMsg.measurement.accZ * imuMsg.measurement.accZ)) *
        RAD_TO_DEG;
    double const pitch =
        atan2(-imuMsg.measurement.accX, imuMsg.measurement.accZ) * RAD_TO_DEG;
    float const filteredRoll =
        kalmanRoll.update(roll, imuMsg.measurement.gyroX);
    float const filteredPitch =
        kalmanPitch.update(pitch, imuMsg.measurement.gyroY);

    Firmata.sendSysex(0x01, 12 * sizeof(float), imuMsg.data);
  }
}
