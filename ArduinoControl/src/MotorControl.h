#pragma once

class MotorControl {
public:
  MotorControl(int const pin1, int const pin2);

  void set(int value);
  void update();

protected:
  void _set();

  int const m_pin1;
  int const m_pin2;

  // 256 == full stop
  int m_currentValue = 256;
  int m_targetValue = 256;
  long m_lastTime = 0;
};