#include "MotorControl.h"

#include <Arduino.h>

static int const stepSize = 5;
static int const stepTime = 10;

/**
 * This masks out some values around 0, where the motor doesn't move.
 * Some experiments have shown that the motor starts moving at about 35 when
 * there is no load.
 * So we mask out values < 20 to have a bigger "stop area".
 */
static int const minMovementValue = 20;

MotorControl::MotorControl(int const pin1, int const pin2)
    : m_pin1(pin1), m_pin2(pin2) {
  pinMode(m_pin1, OUTPUT);
  pinMode(m_pin2, OUTPUT);
}

void MotorControl::set(int value) {
  m_targetValue = value;
  update();
}
void MotorControl::_set() {
  if (m_currentValue < 256 - minMovementValue) {
    digitalWrite(m_pin2, 1);
    analogWrite(m_pin1, m_currentValue);
  } else if (m_currentValue > 256 + minMovementValue) {
    digitalWrite(m_pin1, 1);
    analogWrite(m_pin2, 512 - m_currentValue);
  } else { // if (m_currentValue == 256)
    // stop
    digitalWrite(m_pin1, 1);
    digitalWrite(m_pin2, 1);
  }
}
void MotorControl::update() {
  if (m_currentValue != m_targetValue) {
    if (millis() > m_lastTime + stepTime) {
      m_lastTime = millis();

      if (abs(m_targetValue - m_currentValue) < stepSize) {
        m_currentValue = m_targetValue;
      } else if (m_currentValue < m_targetValue) {
        m_currentValue += stepSize;
      } else {
        m_currentValue -= stepSize;
      }
      _set();
    }
  }
}
